<?php

namespace Drupal\libravatar\Plugin\AvatarGenerator;

use Drupal\Core\Session\AccountInterface;
use Drupal\avatars\Plugin\AvatarGenerator\AvatarGeneratorBase;
use Drupal\libravatar\LibravatarService;

/**
 * Libravatar generated avatars generator.
 *
 * @AvatarGenerator(
 *   id = "libravatar_generator",
 *   label = @Translation("Libravatar generator"),
 *   description = @Translation("Libravatar avatar generator."),
 *   fallback = TRUE,
 *   dynamic = TRUE,
 *   remote = TRUE
 * )
 */
class LibravatarGenerator extends AvatarGeneratorBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function generateUri(AccountInterface $account): string {
    $libravatar = new LibravatarService();
    $libravatar
      ->setAlgorithm('sha256')
      ->setHttps(TRUE);
    return $libravatar->getUrl($account->getEmail());
  }

}
